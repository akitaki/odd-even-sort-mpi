#include <algorithm>
#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <iterator>
#include <limits>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <thread>
#include <type_traits>
#include <vector>

#include <cassert>
#include <cstdlib>

#include <mpi.h>

// Check the return value of the wrapped MPI call
#define checked(expr)                                                          \
    {                                                                          \
        int res = expr;                                                        \
        if (res != MPI_SUCCESS) {                                              \
            throw std::runtime_error(                                          \
                std::string("MPI operation failed: `") + #expr + "`");         \
        }                                                                      \
    }

void radix_sort(float* array_in, float* sorted_in, uint32_t length);
size_t div_up(size_t p, size_t q) { return p / q + (p % q != 0); }

enum class MpiFileMode { Read, Write };
class MpiFile {
  public:
    MpiFile(int comm, const char* filename, MpiFileMode filemode) : comm(comm) {
        int amode = amode_from_mpi_filemode(filemode);
        checked(MPI_File_open(comm, filename, amode, MPI_INFO_NULL, &file));
    }
    ~MpiFile() { MPI_File_close(&file); }
    MpiFile(const MpiFile&) = delete;
    MpiFile& operator=(const MpiFile&) = delete;

    void read_floats(float* buffer, size_t offset, size_t length) {
        checked(MPI_File_read_at(
            file,
            offset * sizeof(float),
            buffer,
            length,
            MPI_FLOAT,
            MPI_STATUS_IGNORE));
    }

    void write_floats(const float* buffer, size_t offset, size_t length) {
        checked(MPI_File_write_at(
            file,
            offset * sizeof(float),
            buffer,
            length,
            MPI_FLOAT,
            MPI_STATUS_IGNORE));
    }

  private:
    MPI_File file;
    int comm;

    static int amode_from_mpi_filemode(MpiFileMode filemode) {
        switch (filemode) {
        case MpiFileMode::Read:
            return MPI_MODE_RDONLY;
        case MpiFileMode::Write:
            return MPI_MODE_WRONLY | MPI_MODE_CREATE;
        }
        assert(false);
    }
};

// Strategy class describing how the local and partner's arrays are partially
// exchanged.
// See LowExchangeStrategy, HighExchangeStrategy, and App::exchange().
class ExchangeStrategy {
  public:
    // Extreme value of this node
    virtual float extreme_of(const std::vector<float>& arr) const = 0;

    // Number of elements to be sent
    virtual size_t send_count(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const = 0;

    // Pointer to starting position of the buffer to be sent
    virtual const float* send_begin(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const = 0;

    // The number of elements that are promised to be kept within this node
    // during this exchange
    size_t keep_count(const std::vector<float>& arr, float partner_ext) const {
        return arr.size() - send_count(arr, partner_ext);
    }
};

class LowExchangeStrategy : public ExchangeStrategy {
  public:
    float extreme_of(const std::vector<float>& arr) const override {
        return arr.back();
    }

    size_t send_count(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const override {
        return std::distance(
            send_begin_iter(arr, partner_ext, partner_keep_count), arr.end());
    }

    const float* send_begin(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const override {
        return &(*send_begin_iter(arr, partner_ext, partner_keep_count));
    }

  private:
    std::vector<float>::const_iterator send_begin_iter(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count) const {

        // send those that are larger than or equal to partner's min
        auto searched_pos = std::upper_bound(
            arr.begin(), arr.end(), partner_ext, std::less_equal<float>());

        size_t max_send_count = arr.size() - partner_keep_count;
        if (std::distance(searched_pos, arr.end()) > max_send_count) {
            // Too many elements, truncate it
            return arr.end() - max_send_count;
        } else {
            return searched_pos;
        }
    }
};

class HighExchangeStrategy : public ExchangeStrategy {
  public:
    float extreme_of(const std::vector<float>& arr) const override {
        return arr.front();
    }

    size_t send_count(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const override {
        return std::distance(
            send_begin_iter(arr, partner_ext, partner_keep_count), arr.rend());
    }

    const float* send_begin(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count = 0) const override {
        return arr.data();
    }

  private:
    // note that this is a reversed one
    std::vector<float>::const_reverse_iterator send_begin_iter(
        const std::vector<float>& arr,
        float partner_ext,
        int partner_keep_count) const {

        // send those that are smaller than or equal to partner's max
        auto searched_pos = std::lower_bound(
            arr.rbegin(), arr.rend(), partner_ext, std::greater_equal<float>());

        size_t max_send_count = arr.size() - partner_keep_count;
        if (std::distance(searched_pos, arr.rend()) > max_send_count) {
            // Too many elements, truncate it
            return arr.rend() - max_send_count;
        } else {
            return searched_pos;
        }
    }
};

class Exchanger {
  public:
    Exchanger(
        int partner,
        std::vector<float>& arr,
        std::vector<float>& recv_arr,
        const ExchangeStrategy& strategy,
        MPI_Comm comm)
        : comm(comm), partner(partner), arr(arr), recv_arr(recv_arr),
          strategy(strategy), send_req(std::make_shared<MPI_Request>(0)),
          recv_req(std::make_shared<MPI_Request>(0)) {}
    ~Exchanger() {}
    Exchanger(const Exchanger&) = delete;
    Exchanger& operator=(const Exchanger&) = delete;

    // Returns partner's extreme value
    std::pair<std::future<void>, std::future<float>> exchange_extreme() {
        auto send_req = std::make_shared<MPI_Request>();
        auto recv_req = std::make_shared<MPI_Request>();

        auto my_ext = std::make_shared<float>(strategy.extreme_of(arr));
        auto partner_ext = std::make_shared<float>(0.0f);

        checked(MPI_Isend(
            my_ext.get(), 1, MPI_FLOAT, partner, 0, comm, send_req.get()));
        checked(MPI_Irecv(
            partner_ext.get(), 1, MPI_FLOAT, partner, 0, comm, recv_req.get()));

        auto send_fut = std::async(std::launch::deferred, [=]() {
            MPI_Wait(send_req.get(), MPI_STATUS_IGNORE);
        });
        auto recv_fut = std::async(std::launch::deferred, [=]() -> float {
            MPI_Wait(recv_req.get(), MPI_STATUS_IGNORE);
            return *partner_ext;
        });

        return {std::move(send_fut), std::move(recv_fut)};
    }

    std::pair<std::future<void>, std::future<int>>
    exchange_keep_count(float partner_ext) {
        auto send_req = std::make_shared<MPI_Request>();
        auto recv_req = std::make_shared<MPI_Request>();

        auto keep_count = std::make_shared<int>(
            static_cast<int>(strategy.keep_count(arr, partner_ext)));
        auto partner_keep_count = std::make_shared<int>(0);

        checked(MPI_Isend(
            keep_count.get(), 1, MPI_INT, partner, 0, comm, send_req.get()));
        checked(MPI_Irecv(
            partner_keep_count.get(),
            1,
            MPI_INT,
            partner,
            0,
            comm,
            recv_req.get()));

        auto send_fut = std::async(std::launch::deferred, [=]() {
            MPI_Wait(send_req.get(), MPI_STATUS_IGNORE);
        });
        auto recv_fut = std::async(std::launch::deferred, [=]() -> int {
            MPI_Wait(recv_req.get(), MPI_STATUS_IGNORE);
            return *partner_keep_count;
        });

        return {std::move(send_fut), std::move(recv_fut)};
    }

    // Returns two futures (send_fut, recv_fut)
    std::pair<std::future<void>, std::future<int>> exchange_buffers(
        float partner_ext, int partner_keep_count, size_t length_per_rank) {
        // Dispatch MPI calls
        checked(MPI_Isend(
            static_cast<const void*>(
                strategy.send_begin(arr, partner_ext, partner_keep_count)),
            static_cast<int>(
                strategy.send_count(arr, partner_ext, partner_keep_count)),
            MPI_FLOAT,
            partner,
            0,
            comm,
            send_req.get()));
        checked(MPI_Irecv(
            static_cast<void*>(recv_arr.data()),
            static_cast<int>(length_per_rank),
            MPI_FLOAT,
            partner,
            0,
            comm,
            recv_req.get()));

        auto send_fut = std::async(std::launch::deferred, [this]() {
            checked(MPI_Wait(send_req.get(), MPI_STATUS_IGNORE));
        });
        auto recv_fut = std::async(std::launch::deferred, [this]() {
            MPI_Status status;
            checked(MPI_Wait(recv_req.get(), &status));

            int recv_count;
            checked(MPI_Get_count(&status, MPI_FLOAT, &recv_count));
            return recv_count;
        });
        return {std::move(send_fut), std::move(recv_fut)};
    }

  private:
    MPI_Comm comm;
    int partner;
    const ExchangeStrategy& strategy;
    std::vector<float>& arr;
    std::vector<float>& recv_arr;
    std::shared_ptr<MPI_Request> send_req, recv_req;
};

class App {
  public:
    App(size_t length, const char* input_filename, const char* output_filename)
        : length(length), input_filename(input_filename),
          output_filename(output_filename) {
        checked(MPI_Comm_size(MPI_COMM_WORLD, &size));
        checked(MPI_Comm_rank(MPI_COMM_WORLD, &rank));
        checked(MPI_Comm_group(MPI_COMM_WORLD, &world_group));

        if (size > length) {
            init_subcomm();
        } else {
            comm = MPI_COMM_WORLD;
        }
    }
    ~App() {}
    App(const App&) = delete;
    App& operator=(const App&) = delete;

    void start() {
        if (inactive) {
            return;
        }

        auto tmp_future = async_vector(length_per_rank(), 0.0);
        auto recv_arr_future = async_vector(length_per_rank(), 0.0);
        auto arr_future = async_vector(
            length_per_rank(), std::numeric_limits<float>::infinity());

        in_file =
            std::make_unique<MpiFile>(comm, input_filename, MpiFileMode::Read);
        out_file = std::make_unique<MpiFile>(
            comm, output_filename, MpiFileMode::Write);

        auto arr = arr_future.get();
        in_file->read_floats(arr.data(), rank_start(), rank_length());

        auto tmp = tmp_future.get();
        radix_sort(arr.data(), tmp.data(), arr.size());
        std::swap(arr, tmp);

        auto recv_arr = recv_arr_future.get();

        for (int phase = 0; phase < size; phase += 1) {
            int partner = calculate_partner(phase);
            int use_count = 0;

            if (partner != MPI_PROC_NULL) {
                if (rank < partner) {
                    LowExchangeStrategy strategy{};
                    Exchanger exchanger{partner, arr, recv_arr, strategy, comm};

                    // float partner_ext = exchanger.exchange_extreme();
                    auto [ext_send_fut, ext_recv_fut] =
                        exchanger.exchange_extreme();
                    float partner_ext = ext_recv_fut.get();
                    auto [keep_send_fut, keep_recv_fut] =
                        exchanger.exchange_keep_count(partner_ext);
                    int partner_keep_count = keep_recv_fut.get();
                    auto [send_fut, recv_fut] = exchanger.exchange_buffers(
                        partner_ext, partner_keep_count, length_per_rank());

                    int keep_count = strategy.keep_count(arr, partner_ext);
                    use_count = merge_low(
                        keep_count,
                        std::move(send_fut),
                        std::move(recv_fut),
                        arr,
                        recv_arr,
                        tmp);

                    ext_send_fut.wait();
                    keep_send_fut.wait();
                } else {
                    HighExchangeStrategy strategy{};
                    Exchanger exchanger{partner, arr, recv_arr, strategy, comm};

                    // float partner_ext = exchanger.exchange_extreme();
                    auto [ext_send_fut, ext_recv_fut] =
                        exchanger.exchange_extreme();
                    float partner_ext = ext_recv_fut.get();
                    auto [keep_send_fut, keep_recv_fut] =
                        exchanger.exchange_keep_count(partner_ext);
                    int partner_keep_count = keep_recv_fut.get();
                    auto [send_fut, recv_fut] = exchanger.exchange_buffers(
                        partner_ext, partner_keep_count, length_per_rank());

                    int keep_count = strategy.keep_count(arr, partner_ext);
                    use_count = merge_high(
                        keep_count,
                        std::move(send_fut),
                        std::move(recv_fut),
                        arr,
                        recv_arr,
                        tmp);

                    ext_send_fut.wait();
                    keep_send_fut.wait();
                }
            }

            int total_use_count = 0;
            checked(MPI_Allreduce(
                &use_count, &total_use_count, 1, MPI_INT, MPI_SUM, comm));
            if (total_use_count == 0 && phase != 0) {
                break;
            }
        }

        out_file->write_floats(arr.data(), rank_start(), rank_length());
    }

  private:
    int length, rank, size;
    const char* input_filename;
    const char* output_filename;
    std::unique_ptr<MpiFile> in_file, out_file;
    MPI_Group world_group, active_group;
    MPI_Comm comm;
    bool inactive = false;

    // Returns the number of used elements in recv_arr
    int merge_low(
        // int recv_count,
        int keep_count,
        std::future<void> send_fut,
        std::future<int> recv_fut,
        std::vector<float>& arr,
        std::vector<float>& recv_arr,
        std::vector<float>& tmp) {

        if (keep_count == arr.size()) {
            send_fut.wait();
            recv_fut.wait();
            return 0;
        }

        // copy those smaller than partner's min
        std::copy(arr.begin(), arr.begin() + keep_count, tmp.begin());

        int recv_count = recv_fut.get();

        // iters of recv_arr
        const auto rit_begin = recv_arr.begin();
        auto rit = rit_begin;
        auto rit_end = recv_arr.begin() + recv_count;

        auto mit = tmp.begin() + keep_count;
        auto lit = arr.begin() + keep_count;
        while (mit != tmp.end()) {
            if (rit != rit_end && *rit <= *lit) {
                *mit = *rit;
                rit++;
                mit++;
            } else if (lit != arr.end()) {
                *mit = *lit;
                lit++;
                mit++;
            } else {
                assert(false);
            }
        }

        send_fut.wait();
        std::swap(arr, tmp);

        return static_cast<int>(std::distance(rit_begin, rit));
    }

    int merge_high(
        int keep_count,
        std::future<void> send_fut,
        std::future<int> recv_fut,
        std::vector<float>& arr,
        std::vector<float>& recv_arr,
        std::vector<float>& tmp) {

        if (keep_count == arr.size()) {
            send_fut.wait();
            recv_fut.wait();
            return 0;
        }

        std::copy(arr.rbegin(), arr.rbegin() + keep_count, tmp.rbegin());

        int recv_count = recv_fut.get();

        // iters of recv_arr
        const auto lit_begin = recv_arr.rend() - recv_count;
        auto lit = lit_begin;
        auto lit_end = recv_arr.rend();

        auto mit = tmp.rbegin() + keep_count;
        auto rit = arr.rbegin() + keep_count;
        while (mit != tmp.rend()) {
            if (lit != lit_end && *lit > *rit) {
                *mit = *lit;
                lit++;
                mit++;
            } else if (rit != arr.rend()) {
                *mit = *rit;
                rit++;
                mit++;
            } else {
                assert(false);
            }
        }
        std::swap(arr, tmp);

        return static_cast<int>(std::distance(lit_begin, lit));
    }

    int calculate_partner(int phase) {
        int partner;
        if (phase % 2 == 0) {
            if (rank % 2 == 0)
                partner = rank + 1;
            else
                partner = rank - 1;
        } else {
            if (rank % 2 == 0)
                partner = rank - 1;
            else
                partner = rank + 1;
        }
        if (partner < 0 || partner >= size || partner >= length) {
            return MPI_PROC_NULL;
        } else {
            return partner;
        }
    }

    // array length & range utilities
    size_t length_per_rank() { return div_up(length, size); }
    size_t rank_length() {
        if (rank == size - 1) {
            return length - length_per_rank() * (size - 1);
        } else {
            return length_per_rank();
        }
    }
    size_t rank_start() { return rank * length_per_rank(); }
    size_t rank_end() { return rank_start() + rank_length(); }

    // Create subcomm to match the input length
    void init_subcomm() {
        std::vector<int> ranks;
        for (int i = 0; i < size; i += 1) {
            ranks.push_back(i);
        }

        if (rank < length) {
            checked(MPI_Group_incl(
                world_group, length, ranks.data(), &active_group));
        } else {
            inactive = true;
            checked(MPI_Group_incl(
                world_group,
                size - length,
                ranks.data() + length,
                &active_group));
        }
        checked(MPI_Comm_create(MPI_COMM_WORLD, active_group, &comm));
        checked(MPI_Comm_size(comm, &size));
        checked(MPI_Comm_rank(comm, &rank));
    }

    static std::future<std::vector<float>>
    async_vector(size_t length, float value) {
        return std::async(std::launch::async, [=]() {
            std::vector<float> vec(length, value);
            return vec;
        });
    }
};

/* radix sort helpers */

uint32_t float_flip(uint32_t f) {
    uint32_t mask = (static_cast<int32_t>(f) >> 31) | 0x80000000;
    return f ^ mask;
}

uint32_t float_flip_inv(uint32_t f) {
    uint32_t mask = (~(static_cast<int32_t>(f) >> 31)) | 0x80000000;
    return f ^ mask;
}

uint32_t part_0(uint32_t f) { return f & 0x7FF; }
uint32_t part_1(uint32_t f) { return (f >> 11) & 0x7FF; }
uint32_t part_2(uint32_t f) { return f >> 22; }

/* radix sort implementation */

void radix_sort(float* array_in, float* sorted_in, uint32_t length) {
    uint32_t* array = reinterpret_cast<uint32_t*>(array_in);
    uint32_t* sorted = reinterpret_cast<uint32_t*>(sorted_in);

    // the histograms
    const uint32_t buckets = 2048;
    std::array<uint32_t, buckets> bucket_0{}, bucket_1{}, bucket_2{};

    // build histograms
    for (size_t i = 0; i < length; i += 1) {
        uint32_t flipped = float_flip(array[i]);

        bucket_0[part_0(flipped)] += 1;
        bucket_1[part_1(flipped)] += 1;
        bucket_2[part_2(flipped)] += 1;
    }

    // convert histograms to prefix sums (of previous elements) in-place
    // these are the starting addresses for each bucket
    std::exclusive_scan(bucket_0.begin(), bucket_0.end(), bucket_0.begin(), 0);
    std::exclusive_scan(bucket_1.begin(), bucket_1.end(), bucket_1.begin(), 0);
    std::exclusive_scan(bucket_2.begin(), bucket_2.end(), bucket_2.begin(), 0);

    // flip and move (array -> sorted), sorted by bits [10:0]
    for (size_t i = 0; i < length; i += 1) {
        uint32_t flipped = float_flip(array[i]);
        uint32_t pos = part_0(flipped);

        sorted[bucket_0[pos]] = flipped;

        // increase the starting address by 1
        bucket_0[pos] += 1;
    }

    // move (sorted -> array), sorted by bits [21:0]
    for (size_t i = 0; i < length; i += 1) {
        uint32_t flipped = sorted[i];
        uint32_t pos = part_1(flipped);

        array[bucket_1[pos]] = flipped;

        // increase the starting address by 1
        bucket_1[pos] += 1;
    }

    // move and flip back (array -> sort), sorted by all bits
    for (size_t i = 0; i < length; i += 1) {
        uint32_t flipped = array[i];
        uint32_t pos = part_2(flipped);

        sorted[bucket_2[pos]] = float_flip_inv(flipped);

        // increase the starting address by 1
        bucket_2[pos] += 1;
    }
}

int main(int argc, char** argv) {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    // Init MPI threaded runtime
    int provided;
    checked(MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided));
    if (provided < MPI_THREAD_MULTIPLE) {
        MPI_Abort(MPI_COMM_WORLD, 2);
    }

    // parse cli args
    int length = std::atoi(argv[1]);
    const char* input_filename = argv[2];
    const char* output_filename = argv[3];

    try {
        App app{static_cast<size_t>(length), input_filename, output_filename};
        app.start();
    } catch (const std::runtime_error& e) {
        std::cerr << "Shutting down: " << e.what() << "\n";
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    checked(MPI_Finalize());

    return EXIT_SUCCESS;
}
