{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    python3
    python3Packages.matplotlib
    python3Packages.autopep8
    pyright
  ];
}
