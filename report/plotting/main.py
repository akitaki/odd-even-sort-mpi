import matplotlib
import matplotlib.pyplot as plt

# Style setup
plt.style.use('ggplot')
pgf_with_pdflatex = {
    'pgf.texsystem': 'pdflatex',
    'pgf.preamble': [
        r'\usepackage[utf8x]{inputenc}',
        r'\usepackage[T1]{fontenc}',
        r'\usepackage{mathpazo}',
        r'\usepackage{tgpagella}',
    ],
    'font.family': 'Tex Gyre Pagella',
    'font.size': 14,
    'mathtext.fontset': 'cm',
}
matplotlib.rcParams.update(pgf_with_pdflatex)


def plot_cumul_time():
    # Data to plot
    allocs = [8913.671, 11329.183, 12387.893, 12843.199]
    presorts = [27595.061, 22575.143, 20930.7, 19532.673]
    merges = [0, 5526.982, 10685.272, 18503.855]
    readfiles = [1575.14, 1347.735, 2138.243, 1592.649]
    writefiles = [5460.266, 10262.414, 26558.395, 33277.11]
    precopies = [0, 0.129, 486.507, 2084.919]
    recv_bufs = [0, 2794.805, 7034.385, 16506.045]

    cpus = [sum(x) for x in zip(allocs, presorts, merges, precopies)]
    ios = [sum(x) for x in zip(readfiles, writefiles)]

    precomms = [sum(x) for x in zip(cpus, ios)]
    comms = [sum(x) for x in zip(recv_bufs)]

    labels = [
        '$1N\\times 1P$',
        '$1N\\times 4P$',
        '$2N\\times 4P$',
        '$4N\\times 4P$'
    ]
    width = 0.35       # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=0.2, bottom=0.2)

    ax.bar(labels, comms, width, bottom=precomms, label='Communication')
    ax.bar(labels, ios, width, bottom=cpus, label='IO')
    ax.bar(labels, cpus, width, label='Computation')

    ax.set_xlabel('Number of nodes and processes')
    ax.set_ylabel('Sum of runtime (ms)')
    ax.set_title('Sum of runtime across all processes')
    ax.legend()

    plt.savefig("times.svg")


def plot_normed_time():
    cores = [1, 4, 8, 16]

    # Data to plot
    allocs = [8913.671, 11329.183, 12387.893, 12843.199]
    presorts = [27595.061, 22575.143, 20930.7, 19532.673]
    merges = [0, 5526.982, 10685.272, 18503.855]
    readfiles = [1575.14, 1347.735, 2138.243, 1592.649]
    writefiles = [5460.266, 10262.414, 26558.395, 33277.11]
    precopies = [0, 0.129, 486.507, 2084.919]
    recv_bufs = [0, 2794.805, 7034.385, 16506.045]

    cpus = [sum(x[1:]) / x[0]
            for x in zip(cores, allocs, presorts, merges, precopies)]
    ios = [sum(x[1:]) / x[0] for x in zip(cores, readfiles, writefiles)]

    precomms = [sum(x) for x in zip(cpus, ios)]
    comms = [sum(x[1:]) / x[0] for x in zip(cores, recv_bufs)]

    labels = [
        '$1N\\times 1P$',
        '$1N\\times 4P$',
        '$2N\\times 4P$',
        '$4N\\times 4P$'
    ]
    width = 0.35       # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()
    fig.subplots_adjust(left=0.2, bottom=0.2)

    ax.bar(labels, comms, width, bottom=precomms, label='Communication')
    ax.bar(labels, ios, width, bottom=cpus, label='IO')
    ax.bar(labels, cpus, width, label='Computation')

    ax.set_xlabel('Number of nodes and processes')
    ax.set_ylabel('Runtime (ms)')
    ax.set_title('Normalized runtime per processes')
    ax.legend()

    plt.savefig("normed.svg")


def plot_total_time():
    # Data to plot

    cores = [1, 4, 8, 16]
    times = [37616.709, 12457.161, 11698.934, 9241.804]

    fig, ax = plt.subplots()
    ax.plot(cores, times, marker='o')
    fig.subplots_adjust(left=0.2, bottom=0.2)

    ax.set_ylabel('Runtime (ms)')
    ax.set_xlabel('Number of processes')
    ax.set_title('Time elapsed')

    plt.savefig("total.svg")


def plot_speedup():
    # Data to plot

    cores = [1, 4, 8, 16]
    times = [37616.709, 12457.161, 11698.934, 9241.804]
    speedups = [times[0] / t for t in times]

    fig, ax = plt.subplots()
    ax.plot(cores, speedups, marker='o')
    ax.plot(cores, cores, '--')
    fig.subplots_adjust(left=0.2, bottom=0.2)
    ax.axis('equal')

    ax.set_ylabel('Speedup')
    ax.set_xlabel('Number of processes')
    ax.set_title('Speedup factors')

    plt.savefig("speedup.svg")


plot_cumul_time()
plot_normed_time()
plot_total_time()
plot_speedup()
