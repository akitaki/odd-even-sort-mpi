{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    clang-tools
    clang_11
    llvm_11
    llvmPackages_11.libstdcxxClang
    gnumake
    cmake
    bear
    texlab
    inkscape
  ];
}
